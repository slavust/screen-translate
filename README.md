# screen-translate
Python 3 script that translates and says closest to cursor word

WARNING: uses Google Translate and Google Text To Speech so internet connection is required

# Usage

1. Install all required dependencies :)
2. See `screen-translate.py --help`


# Author

Viacheslav Ustymenko

website: [https://slavust.name](https://slavust.name)

e-mail: homer@slavust.name
