#!/usr/bin/python3

import pytesseract
import cv2
import numpy as np
import math
import pyautogui
import os.path
import os
from gtts import gTTS
from playsound import playsound
import tempfile
import argparse
import appdirs
import configparser
import sys
from pynput import keyboard
import googletrans

APPNAME = 'screen-translate'
AUTHOR = 'Slavust'
SETTINGS_FILE = 'settings.ini'

def distance_to_bbox(pt, bbox):
    dx = np.max([bbox['min'][0] - pt[0], 0, pt[0] - bbox['max'][0]])
    dy = np.max([bbox['min'][1] - pt[1], 0, pt[1] - bbox['max'][1]])
    return math.sqrt(dx**2 + dy**2)


def trunc_nonalpha(word):
    start_text = 0
    for i in range(len(word)):
        if word[i].isalpha():
            start_text = i
            break
    end_text = len(word)
    for i in range(len(word)-1, start_text, -1):
        if word[i].isalpha():
            end_text = i+1
            break
    return word[start_text:end_text]


def get_closest_word_on_img(img, pos):
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    gray, binary = cv2.threshold(gray, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)

    pixel_count = binary.shape[0]*binary.shape[1]
    white_count = np.count_nonzero(binary)

    if white_count < pixel_count / 2:
        binary = cv2.bitwise_not(binary)

    distances = []
    words = []
    text_data = pytesseract.image_to_data(binary, output_type='dict')
    for i in range(len(text_data['level'])):
        if len(text_data['text'][i]) == 0:
            continue
        words.append(text_data['text'][i])
        (x, y, w, h) = (text_data['left'][i], 
                        text_data['top'][i], 
                        text_data['width'][i], 
                        text_data['height'][i])
        bbox = {'min': (x, y), 'max': (x+w, y+h)}
        distances.append(distance_to_bbox(pos, bbox))

    if len(words) == 0:
        return ''

    min_distance_indx, _ = min(enumerate(distances), key=lambda x: x[1])
    word = words[min_distance_indx]
    word = trunc_nonalpha(word)
    return word


def crop_screenshot(screenshot, size_percents, cursor_pos):
    new_sz = [int(screenshot.shape[0]*size_percents/100.0), int(screenshot.shape[1]*size_percents/100.0)]
    half_sz = [int(sz * 0.5) for sz in new_sz]
    minimum = (max(cursor_pos[1] - half_sz[0], 0), max(cursor_pos[0] - half_sz[1], 0))
    maximum = (min(cursor_pos[1] + half_sz[0], screenshot.shape[0]), min(cursor_pos[0] + half_sz[1], screenshot.shape[1]))
    return screenshot[minimum[0]: maximum[0],
                      minimum[1]: maximum[1], :], np.array([cursor_pos[0] - minimum[1], cursor_pos[1] - minimum[0]])



def translate_word(word, dest_code):
    if not word:
        return ''
    translator = googletrans.Translator()
    return translator.translate(word, dest=dest_code).text


def say_word(word, lang_code):
    tts = gTTS(text=word, lang=lang_code)
    word_sound_path = os.path.join(tempfile.gettempdir(), 'screen-translate.mp3')
    tts.save(word_sound_path)
    playsound(word_sound_path)
    os.remove(word_sound_path)


def one_translate(to_lang_code):
    mouse_pos = pyautogui.position()
    mouse_pos = (mouse_pos[0], mouse_pos[1])
    screenshot = np.array(pyautogui.screenshot())
    
    screenshot, mouse_pos = crop_screenshot(screenshot, 10, mouse_pos)
    word = get_closest_word_on_img(screenshot, mouse_pos)
    word = trunc_nonalpha(word)
    translated_word = translate_word(word, to_lang_code)
    if word:
        say_word(translated_word, to_lang_code)


def list_available_translation_languages():
    for lang in googletrans.LANGCODES.values():
        print(lang)


def set_target_language(language_code):
    if language_code not in googletrans.LANGCODES.values():
        print('Specified language not available')
        return

    data_dir = appdirs.user_data_dir(APPNAME, AUTHOR)
    settings_file = os.path.join(data_dir, SETTINGS_FILE)

    config = configparser.ConfigParser()
    config.read(settings_file)
    if 'LANGUAGES' not in config:
        config['LANGUAGES'] = {'target': language_code}
    config['LANGUAGES']['target'] = language_code

    if not os.path.exists(data_dir):
        os.makedirs(data_dir)
    with open(settings_file, 'w') as configfile:
        config.write(configfile)


def load_target_language():
    data_dir = appdirs.user_data_dir(APPNAME, AUTHOR)
    settings_file = os.path.join(data_dir, SETTINGS_FILE)

    config = configparser.ConfigParser()
    config.read(settings_file)
    to_code = config['LANGUAGES'].get('target', 'uk')

    return to_code


def translate():
    target_language_code = load_target_language()
    ctrl_pressed = False
    def on_press(key):
        global ctrl_pressed
        try:
            character = key.char
            if ctrl_pressed and character == 't':
                one_translate(target_language_code)
                ctrl_pressed = False
        except AttributeError:
            if key == keyboard.Key.ctrl:
                ctrl_pressed = True
    def on_release(key):
        global ctrl_pressed
        if key == keyboard.Key.ctrl:
            ctrl_pressed = False
    listener = keyboard.Listener(on_press=on_press, on_release=on_release)
    listener.start()
    listener.join()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--list-languages', dest='list_languages', action='store_true', help='List available translators')
    parser.set_defaults(list_languages=False)
    parser.add_argument('--set-target-lang', type=str, default='', help='Language to which translate')
    
    args = parser.parse_args()

    if args.list_languages:
        list_available_translation_languages()
        sys.exit(0)
    elif args.set_target_lang:
        set_target_language(args.set_target_lang)
        sys.exit(0)
    else:
        while True:
            try:
                translate()
            except Exception as e:
                continue
